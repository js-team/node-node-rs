# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.6.0](https://github.com/napi-rs/node-rs/compare/@node-rs/deno-lint@0.5.0...@node-rs/deno-lint@0.6.0) (2020-09-04)

**Note:** Version bump only for package @node-rs/deno-lint

# [0.5.0](https://github.com/napi-rs/node-rs/compare/@node-rs/deno-lint@0.4.2...@node-rs/deno-lint@0.5.0) (2020-09-03)

### Features

- **deno-lint:** upgrade deno_lint swc ([5aff9ab](https://github.com/napi-rs/node-rs/commit/5aff9ab0986e5274f60d73148f6bc194112d036a))

## [0.4.2](https://github.com/napi-rs/node-rs/compare/@node-rs/deno-lint@0.4.1...@node-rs/deno-lint@0.4.2) (2020-08-31)

### Bug Fixes

- **deno-lint:** add tsx: true when path endsWith .tsx ([b1e5502](https://github.com/napi-rs/node-rs/commit/b1e5502851f78469d15e7ad9b0d4deb3c87e2f59))

## [0.4.1](https://github.com/napi-rs/node-rs/compare/@node-rs/deno-lint@0.3.0...@node-rs/deno-lint@0.4.1) (2020-08-31)

### Bug Fixes

- **deno-lint:** remove debug print ([9be8e9a](https://github.com/napi-rs/node-rs/commit/9be8e9aa2bc0b594e072df0141ab95791ef53553))

### Features

- **deno-lint:** respect .denolintignore file ([5e2f12a](https://github.com/napi-rs/node-rs/commit/5e2f12af23bdb79beefc187d025d6d57041599f1))

# [0.4.0](https://github.com/napi-rs/node-rs/compare/@node-rs/deno-lint@0.3.0...@node-rs/deno-lint@0.4.0) (2020-08-31)

### Features

- **deno-lint:** respect .denolintignore file ([5e2f12a](https://github.com/napi-rs/node-rs/commit/5e2f12af23bdb79beefc187d025d6d57041599f1))

# [0.3.0](https://github.com/napi-rs/node-rs/compare/@node-rs/deno-lint@0.2.1...@node-rs/deno-lint@0.3.0) (2020-08-27)

### Features

- **deno_lint:** upgrade deno-lint ([8c664f1](https://github.com/napi-rs/node-rs/commit/8c664f1db68e78d1b6e06c0adcd021d49ad11255))

# 0.1.0 (2020-08-18)

### Bug Fixes

- **deno-lint:** publish script ([a2d789f](https://github.com/napi-rs/node-rs/commit/a2d789fdb649fdf88385da229b99365d7d007b78))

### Features

- **deno-lint:** implment webpack loader and cli ([59454e2](https://github.com/napi-rs/node-rs/commit/59454e24f352a3333197fe5b20f90db753845a8b))
- respect .eslintignore and .gitignore file ([9e1c24a](https://github.com/napi-rs/node-rs/commit/9e1c24a9cc0a9cd53f732c72fd48510db5000b43))
- start deno lint ([025abfb](https://github.com/napi-rs/node-rs/commit/025abfb754dc56ff0f27c2af6370c4f529b5ccb6))
