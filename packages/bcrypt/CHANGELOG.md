# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://github.com/napi-rs/node-rs/compare/@node-rs/bcrypt@0.4.1...@node-rs/bcrypt@0.5.0) (2020-09-04)

**Note:** Version bump only for package @node-rs/bcrypt

## [0.3.3](https://github.com/napi-rs/node-rs/compare/@node-rs/bcrypt@0.3.2...@node-rs/bcrypt@0.3.3) (2020-08-18)

**Note:** Version bump only for package @node-rs/bcrypt
