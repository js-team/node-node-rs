# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.1](https://github.com/napi-rs/node-rs/compare/@node-rs/crc32@0.5.0...@node-rs/crc32@0.5.1) (2020-09-08)

### Performance Improvements

- **crc32:** avoid useless typeof ([345fc04](https://github.com/napi-rs/node-rs/commit/345fc04f8b9e4d56b73d51ab4b3254f581fc86cb))

# [0.5.0](https://github.com/napi-rs/node-rs/compare/@node-rs/crc32@0.4.1...@node-rs/crc32@0.5.0) (2020-09-04)

**Note:** Version bump only for package @node-rs/crc32

## [0.3.3](https://github.com/napi-rs/node-rs/compare/@node-rs/crc32@0.3.2...@node-rs/crc32@0.3.3) (2020-08-18)

**Note:** Version bump only for package @node-rs/crc32
