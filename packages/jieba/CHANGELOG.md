# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.6.0](https://github.com/napi-rs/node-rs/compare/@node-rs/jieba@0.5.1...@node-rs/jieba@0.6.0) (2020-09-04)

**Note:** Version bump only for package @node-rs/jieba

# [0.4.0](https://github.com/napi-rs/node-rs/compare/@node-rs/jieba@0.4.0-alpha.1...@node-rs/jieba@0.4.0) (2020-08-18)

### Features

- **jieba:** bump to stable jieba-rs ([f412b49](https://github.com/napi-rs/node-rs/commit/f412b49776091aa5713e2881fc88eafc5d647c82))
