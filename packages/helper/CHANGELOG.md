# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.4.0](https://github.com/napi-rs/node-rs/compare/@node-rs/helper@0.3.1...@node-rs/helper@0.4.0) (2020-09-04)

**Note:** Version bump only for package @node-rs/helper
